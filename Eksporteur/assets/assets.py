import bpy
import bmesh
from . import generic_functions as g
from bpy.types import PropertyGroup
from bpy.props import StringProperty, BoolProperty, EnumProperty, FloatProperty


# Could be objects or collections
class LOD_Object(bpy.types.PropertyGroup):
    obj_pointer = bpy.props.PointerProperty(name="Object", type=bpy.types.Object)

# Collections
class LOD_Collection(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(name="Collection name", default="Collection name")

class LOD(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(name="name", default="LOD")

    # Content
    objects = bpy.props.CollectionProperty(type=LOD_Object, name="Contents")
    collections = bpy.props.CollectionProperty(type=LOD_Collection, name="Collections")

    active_lod_object_index = bpy.props.IntProperty(name="Active LOD Object", default=-1)
    active_lod_collection_index = bpy.props.IntProperty(name="Active LOD Collection", default=-1)

    @classmethod
    def calculate_vertcount(self, context) -> int:
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        objects = asset.get_lod_objects(context)

        vert_count = 0
        for ob in objects:
            vert_count += g.CalculateObjectVertexCount(ob)

        return vert_count

    @classmethod
    def calculate_tricount(self, context) -> int:
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        objects = asset.get_lod_objects(context)

        total_triangles = 0

        for ob in objects:
            total_triangles += g.CalculateObjectTriangleCount(ob)

        return total_triangles


    @classmethod
    def remove_object_by_index(self, context, index):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        asset.lods[asset.active_lod_index].objects.remove(index)
        asset.lods[asset.active_lod_index].active_lod_object_index -= 1

class Asset(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(name="Name", default="Asset")
    active_lod_index = bpy.props.IntProperty(name="Active LOD", default=-1)
    lods = bpy.props.CollectionProperty(type=LOD, name="LODs")

    @classmethod
    def calculate_overall_polycount(self, context, model_index : int) -> int:
        settings = context.scene.export_settings
        asset = settings.assets[model_index]

        polycount = 0
        for idx, lod in enumerate(asset.lods):
            polycount += lod.calculate_tricount(context)

        return polycount

    @classmethod
    def get_lod_objects(self, context, lod_index = -1) -> []:
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        if lod_index == -1:
            lod_index = asset.active_lod_index
        lod = asset.lods[lod_index]

        objects = []
        # First off, find LOD collections.
        for collection in lod.collections:
            col = g.FindCollection(collection.name)
            if col:
                collection_objects = g.GetNestedObjectsFromCollection(col, 0, False)
                objects.extend(collection_objects)

        # Then try to add separate objects if they don't belong
        # to any LOD collection.
        for lod_object in lod.objects:
            if lod_object not in objects:
                objects.append(lod_object.obj_pointer)

        return objects

    @classmethod
    def set_lod_index(self, context, value: int):
        settings = context.scene.export_settings
        settings.assets[settings.active_asset_index].active_lod_index = value

    @classmethod
    def get_lod_index(self, context) -> int:
        settings = context.scene.export_settings
        return settings.assets[settings.active_asset_index].active_lod_index

    @classmethod
    def get_current_lod(self, context) -> LOD:
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        #return self.lods[index - 1]
        return asset.lods[asset.active_lod_index]

    @classmethod
    def get_lod_by_index(self, index: int) -> LOD:
        return self.lods[index]

    @classmethod
    def add_lod(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        new_lod = settings.assets[settings.active_asset_index].lods.add()

        index = len(settings.assets[settings.active_asset_index].lods) - 1
        self.set_lod_index(context, index)

        #print("New LOD index: " + str(index))

        # @Todo: Reuse naming functionality with SyncLodNames function.
        suggested_name = settings.get_current_asset(context).name + "_lod" + str(len(self.lods) - 1)
        new_lod.name = g.GenerateLODName(asset.name, context, index, False)
        #new_lod.name = suggested_name


    @classmethod
    def remove_lod(self, context, index: int):
        settings = context.scene.export_settings
        settings.assets[settings.active_asset_index].lods.remove(index)

        asset = settings.assets[settings.active_asset_index]
        asset.active_lod_index -= 1

    @classmethod
    def clear_lods(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]

        for i in range(1, len(asset.lods)):
            asset.lods.remove(1)
        asset.active_lod_index = 0

    @classmethod
    def does_have_lods(self, context) -> bool:
        settings = context.scene.export_settings
        return len(settings.assets[settings.active_asset_index].lods) > 1

    @classmethod
    def init(self, context):
        print("init")

class ExportSettings(bpy.types.PropertyGroup):
    assets = bpy.props.CollectionProperty(type=Asset, name="Assets")
    active_asset_index = bpy.props.IntProperty(name="Active Asset", default=-1)

    export_mode = EnumProperty(
    name="Export Mode",
    items=(
        ('Collections', 'Collections', 'Treat collections as main tool for managing assets.'),
        ('Empties', 'Empties', 'Use parent-child relationships and empties as origins for export.')
    ),
    default="Collections")

    export_scale = FloatProperty(name='Scale', default = 1.0)

    @classmethod
    def get_current_asset(self, context) -> Asset:
        settings = context.scene.export_settings
        return settings.assets[settings.active_asset_index]


def register():
    for cls in (LOD_Object, LOD_Collection, LOD, Asset, ExportSettings):
        bpy.utils.register_class(cls)
    bpy.types.Scene.export_settings = bpy.props.PointerProperty(type=ExportSettings)


def unregister():
    for cls in (LOD_Object, LOD_Collection, LOD, Asset, ExportSettings):
        bpy.utils.unregister_class(cls)
    del bpy.types.Scene.export_settings


if __name__ == "__main__":
    register()
