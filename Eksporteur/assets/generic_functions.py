import bpy
import bmesh
from bpy.props import IntProperty

from . import assets as a

def GenerateLODName(base_name: str, context, index: int, last: bool = False) -> str:

    prefix = ""
    postfix = ""
    if context.scene.lod_name_lod0_ignore_postfix and index == 0:
        if context.scene.lod_name_use_prefix:
            prefix = context.scene.lod_name_prefix + '_'
        name = "{prefix}{name}".format(prefix = prefix, name = base_name)
        return name


    lod_name = "lod"
    if context.scene.lod_names_capitalized:
        lod_name = "LOD"
    if context.scene.lod_name_use_prefix:
        prefix = context.scene.lod_name_prefix + '_'
    idx_str = str(index)
    if last and context.scene.lod_name_proxy and index != 0:
        if context.scene.lod_names_capitalized:
            idx_str = "X"
        else:
            idx_str = "x"
    postfix = lod_name + idx_str
    name = "{prefix}{name}_{postfix}".format(prefix = prefix, name = base_name, postfix = postfix)
    return name

def GetFilePath(context, subfolder: str = None) -> str:

    if context.scene.specific_export_path:
        filepath = context.scene.content_folder
    else:
        filepath = bpy.path.abspath("//")

    filepath = filepath + "\\"
    if context.scene.folder_per_asset:
        filepath += subfolder
    return filepath

def GetNestedObjectsFromCollection(collection, depth: int, include_hidden: bool) -> []:
    objects = []

    #if collection.hide_viewport:
    if collection.hide_viewport:
        if not include_hidden:
            return objects

    # Direct objects of the collection.
    objects.extend(o for o in collection.objects if o.type != "CAMERA" or o.type != "LIGHT")

    # Dive into other child collections to retrieve their objects.
    for col in collection.children:
        nested_objects = GetNestedObjectsFromCollection(col, depth, False)
        objects.extend(nested_objects)

    return objects

def GetNestedCollections(collection) -> []:
    collections = []
    collections.append(collection)

    for col in collection.children:
        nested_collections = GetNestedCollections(col)
        collections.extend(nested_collections)

    return collections

def RecursiveSearchLayerCollection(layer_collection, collection_name):
    found = None
    if (layer_collection.name == collection_name):
        return layer_collection
    for layer in layer_collection.children:
        found = RecursiveSearchLayerCollection(layer, collection_name)
        if found:
            return found

def CalculateObjectTriangleCount(obj) -> int:
    total_triangles = 0
    if obj.type == 'MESH':
        for face in obj.data.polygons:
            vertices = face.vertices
            triangles = len(vertices) - 2
            total_triangles += triangles
    return total_triangles

    if obj.type == 'MESH' and len(obj.modifiers) > 0:
        mesh = bmesh.new()
        depsgraph = bpy.context.evaluated_depsgraph_get()
        mesh.from_object(obj, depsgraph)
        for face in mesh.faces:
            vertices = face.verts
            triangles = len(vertices) - 2
            total_triangles += triangles
        mesh.free()
    else:
        if obj.type == 'MESH':
            for face in obj.data.polygons:
                vertices = face.vertices
                triangles = len(vertices) - 2
                total_triangles += triangles
        else:
            total_triangles = 666

    return total_triangles

def CalculateObjectVertexCount(obj) -> int:
    vert_count = 0

    if obj.type == 'MESH':
        vert_count += len(obj.data.vertices)
    return vert_count

    if obj.type == 'MESH' and len(obj.modifiers) > 0:
        mesh = bmesh.new()
        depsgraph = bpy.context.evaluated_depsgraph_get()
        mesh.from_object(obj, depsgraph)
        vert_count += len(mesh.verts)
        mesh.free()
    else:
        if obj.type == 'EMPTY':
            vert_count = 999;
        elif obj.type == 'MESH':
            vert_count += len(obj.data.vertices)

    return vert_count

def FindCollection(collection_name: str, create_if_none: bool = False):
    if bpy.data.collections.get(collection_name) is None:
        if create_if_none:
            return bpy.data.collections.new(collection_name)
        else:
            return None
    return bpy.data.collections.get(collection_name)

def GetViewportOrderedCollections(context):
    def fn(c, out, addme):
        if addme:
            out.append(c)
        for c1 in c.children:
            out.append(c1)
        for c1 in c.children:
            fn(c1, out, False)
    collections = []
    fn(context.scene.collection, collections, True)
    return collections

def GetAreaFromContext(context, area_type):
    area = None
    for a in context.screen.areas:
        if a.type == area_type:
            area = a
            break
    return area

def SetCollectionViewportVisibility(context, collection_name, visibility=True):
    collections = GetViewportOrderedCollections(context)

    collection = None
    index = 0
    for c in collections:
        if c.name == collection_name:
            collection = c
            break
        index += 1

    if collection is None:
        return

    first_object = None
    if len(collection.objects) > 0:
        first_object = collection.objects[0]

    try:
        bpy.ops.object.hide_collection(context, collection_index=index, toggle=True)

        if first_object.visible_get() != visibility:
            bpy.ops.object.hide_collection(context, collection_index=index, toggle=True)
    except:
        context_override = context.copy()
        context_override['area'] = GetAreaFromContext(context, 'VIEW_3D')

        bpy.ops.object.hide_collection(context_override, collection_index=index, toggle=True)

        if first_object:
            if first_object.visible_get() != visibility:
                bpy.ops.object.hide_collection(context_override, collection_index=index, toggle=True)

    return collection
