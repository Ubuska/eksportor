import bpy
import os
import sys

if 'DEBUG_MODE' in sys.argv:
    from assets import assets as dt
else:
    from ..assets import assets as a

class LODExportFBX(bpy.types.Operator):
    bl_idname = "eksportor.lod_file_export"
    bl_label = "Export Asset FBX"
    bl_options = set()

    filepath = bpy.props.StringProperty(name='FilePath', default="")
    lod_name = bpy.props.StringProperty(name='LODName', default="")

    def execute(self, context):

        settings = context.scene.export_settings
        if not os.path.exists(self.filepath):
            os.mkdir(self.filepath)

        filepath_full = self.filepath + '\\' + self.lod_name + '.fbx'

        bpy.ops.export_scene.fbx(filepath=filepath_full, axis_forward='-Z', axis_up='Y', use_selection=True, use_mesh_modifiers=True, object_types = {'EMPTY', 'MESH', 'OTHER'})

        print("LOD :: " + filepath_full + " exported.")

        if context.scene.open_folder_after_export:
            bpy.ops.eksportor.open_export_folder('INVOKE_DEFAULT')

        return {'FINISHED'}

class LODImportFBX(bpy.types.Operator):
    bl_idname = "eksportor.lod_file_import"
    bl_label = "Import Asset FBX"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        return {'FINISHED'}


def register():
    for cls in (LODExportFBX, LODImportFBX):
        bpy.utils.register_class(cls)
