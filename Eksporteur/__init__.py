import sys
import importlib

bl_info = {
    "name": "Eksportor",
    "description": "Assets Exporter.",
    "author": "Peter Gubin",
    "version": (1, 1, 0),
    "blender": (2, 91, 0),
    "location": "Scene Tab in Properties.",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"}

modulesNames = ['assets.assets',
'assets.generic_functions',
'panels.assets_export_panel',
'operators.asset_io_operators'
]

if 'DEBUG_MODE' in sys.argv:
    from assets import *
    from operators import *
    from panels import *
else:
    from .assets import *
    from .operators import *
    from .panels import *

modulesFullNames = {}
for currentModuleName in modulesNames:
    if 'DEBUG_MODE' in sys.argv:
        modulesFullNames[currentModuleName] = ('{}'.format(currentModuleName))
    else:
        modulesFullNames[currentModuleName] = ('{}.{}'.format(__name__, currentModuleName))

for currentModuleFullName in modulesFullNames.values():
    if currentModuleFullName in sys.modules:
        importlib.reload(sys.modules[currentModuleFullName])
    else:
        globals()[currentModuleFullName] = importlib.import_module(currentModuleFullName)
        setattr(globals()[currentModuleFullName], 'modulesNames', modulesFullNames)

def register():
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'register'):
                sys.modules[currentModuleName].register()

def unregister():
    for currentModuleName in modulesFullNames.values():
        if currentModuleName in sys.modules:
            if hasattr(sys.modules[currentModuleName], 'unregister'):
                sys.modules[currentModuleName].unregister()

if __name__ == "__main__":
    register()
