﻿import bpy
import subprocess
from bpy.props import IntProperty
from math import *
from mathutils import Matrix, Vector, Euler


import sys

if 'DEBUG_MODE' in sys.argv:
    from assets import assets as dt
    from assets import generic_functions as g
    from operators import asset_io_operators
else:
    from ..assets import assets as a
    from ..assets import generic_functions as g
    from ..operators import asset_io_operators

class ExportWarning:
    title = "Reason"
    description = "Description"

class Asset_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index: int):
        settings = context.scene.export_settings
        asset = settings.assets[index]
        lods_number = len(asset.lods)

        layout.operator("eksportor.asset_move_up", text="", icon='SORT_DESC').index = index
        layout.operator("eksportor.asset_move_down", text="", icon='SORT_ASC').index = index
        if bpy.context.scene.export_settings.export_mode == 'Empties':
            layout.operator("eksportor.asset_focus", text="", icon='TRACKING').asset_index = index
        else:
            layout.operator("eksportor.asset_isolate", text="", icon='VIS_SEL_11').asset_index = index
        split = layout.split(factor=0.70, align=False)
        polycount = asset.calculate_overall_polycount(context, index)

        #name = "[" + str(lods_number) + " LOD"
        name = str(lods_number) + " LOD"

        if lods_number > 1:
            name += "s"
        #name += ", polys: "
        #name += str(polycount)
        #name += "]"

        col = split.column()
        split.label(text=name)
        col.prop(item, "name", text="")
        layout.operator("eksportor.asset_export", text="EXPORT", icon='FORWARD').index = index
        layout.operator("eksportor.asset_remove", text="", icon='X').index = index

class LOD_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index: int):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]

        layout.operator("eksportor.lod_isolate", text="", icon='VIS_SEL_11').lod_index = index
        index_label = index
        label = "LOD{index}".format(index = index_label)
        layout.label(text=label)
        split1 = layout.split(factor=0.3)
        split2 = layout.split(factor=0.7)
        col1, col2 = (split1.row(),split1.row())


        col1.operator("eksportor.asset_lod_collection_add", text=" COL", icon='EVENT_C').lod_index = index

        col1.operator("eksportor.asset_lod_object_include", text=" OBJ", icon='EVENT_O')

        col2.prop(item, "name", text="")


        export_selected = layout.operator("eksportor.asset_lod_export", text="", icon='FORWARD')
        export_selected.lod_index = index
        export_selected.asset_index = settings.active_asset_index
        row = layout.row()
        row.operator("eksportor.asset_lod_remove", text="", icon='X').index = index
        if index == 0:
            row.enabled = False

class LOD_Objects_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index: int):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        lod = asset.lods[asset.active_lod_index]

        obj = lod.objects[index].obj_pointer
        if obj is None:
            row = layout.row()
            row.label(text="NONE")
        split1 = layout.split(factor=1.0)
        split2 = layout.split(factor=0.5)
        col1, col2 = (split1.row(),split1.row())
        row1, row2 = (split2.row(),split2.row())
        #col1.prop(obj, "name", text="")

        vertcount = g.CalculateObjectVertexCount(obj)
        tricount = g.CalculateObjectTriangleCount(obj)
        col1.label(text="[v: {vertcount}, tr: {tricount}]".format(vertcount = vertcount, tricount = tricount))
        #layout.label(text="Test")
        mesh = item.obj_pointer.data
        #col2.label(text="tris: " + str(len(mesh.polygons)))
        #col2.label(text="verts: " + str(len(mesh.vertices)))
        row2.operator("eksportor.asset_lod_object_select").index = index
        row2.operator("eksportor.asset_lod_object_remove").index = index

class LOD_Collections_List(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):

        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        lod = asset.lods[asset.active_lod_index]

        layout.label(text=item.name)
        col = g.FindCollection(item.name)

        if col:
            objects = g.GetNestedObjectsFromCollection(col, 0, False)
            #objects = col.objects
            #layout.label(text="COUNT = {c}".format(c=str(len(objects))))
            column = layout.column()
            for obj in objects:
                vertcount = g.CalculateObjectVertexCount(obj)
                tricount = g.CalculateObjectTriangleCount(obj)
                label_text = "{name}   [v: {verts}, tr: {tris}]".format(name = obj.name, verts = vertcount, tris = tricount)

                bAddedAsSeparate = False
                for ob in lod.objects:
                    if ob.obj_pointer.name == obj.name:
                        bAddedAsSeparate = True

                if bAddedAsSeparate:
                    label_text += "  <-  Separate"
                column.label(text=label_text)
                #column.operator("cod.xmodel_lod_content_select").index = index
            layout.operator("eksportor.asset_lod_collection_select").index = index
        else:
            layout.label(text="No collection found")

        layout.operator("eksportor.asset_lod_collection_remove").index = index

        #layout.operator("cod.xmodel_lod_collection_select").index = index
        #layout.operator("cod.xmodel_lod_collection_remove").index = index

class Eksporteur_Export_Base(bpy.types.Panel):
    bl_label = "Base"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"

    def sync_lod_indexes(self, context):
        settings = context.scene.export_settings
        for idx, lod in enumerate(settings.assets[settings.active_asset_index].lods):
            lod.index = idx

    def show_warning(self, layout, warning):
        box = layout.box()
        row = box.row()
        row.label(text=warning.description)

    @staticmethod
    def check_lod_is_empty(context, index):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]

        if len(asset.get_lod_objects(context, index)) == 0:
            warning_lod_is_empty = Warning()
            warning_lod_is_empty.description = "LOD" + str(index) + " is empty."
            return warning_lod_is_empty

    @staticmethod
    def check_are_lods_descending(context):
        is_descending = True

        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]

        for idx, lod in enumerate(asset.lods):
            if idx > 0:
                polycount_current = lod.calculate_tricount(context)
                #previous_lod = asset.get_lod_by_index(idx - 1)
                previous_lod = asset.lods[idx - 1]
                polycount_last = previous_lod.calculate_tricount(context)

                # @Todo: Could be more sophisticated.
                if not polycount_current < polycount_last:
                    is_descending = False

        if not is_descending:
            warning = Warning()
            warning.description = "LODs descending hierarchy is wrong."
            return warning
        return None

    def check_too_many_lods(self, asset):
        if len(asset.lods) > 5:
            warning = Warning()
            warning.description = "Too many lods, consider lowering the amount to less than 5."
            return warning
        return None

    def draw(self, context):
        layout = self.layout
        scene = context.scene

        row = layout.row()
        row.operator("eksportor.asset_export_all")

        box = layout.box()

        if scene.export_options_show:
            box.prop(scene, "export_options_show", icon="TRIA_DOWN", emboss=False)

            box_export = box.box()
            #row = box_export.row()
            #row.label(text="Export")

            row = box_export.row()

            specific_export_path_title = "Use Blend file location"
            if scene.specific_export_path:
                specific_export_path_title = "Specify export path below:"
            row.prop(scene, "specific_export_path", toggle=True, text=specific_export_path_title)
            row = box_export.row()
            #row.label(text="Export Path:")

            row = box_export.row()

            split1 = row.split(factor=0.7)
            split2 = row.split(factor=0.3)
            col1, col2 = (split1.row(),split1.row())

            if scene.specific_export_path:
                col1.prop(scene, "content_folder", text="")
                col2.operator("eksportor.open_export_folder")
            else:
                col1.label(text=bpy.path.abspath("//"))
                col2.operator("eksportor.open_export_folder")

            row = box_export.row()
            row.prop(scene, "folder_per_asset")
            row.prop(scene, "open_folder_after_export")

            row = box_export.row(align=True)
            row.use_property_split = False
            if bpy.context.scene.export_settings.export_mode == 'Empties':
                row.prop(scene, "move_to_world_origin", toggle=True)
            row.prop(scene, "clear_rotation", toggle=True)


            row = box_export.row(align=True)
            row.use_property_split = False
            row.prop(scene, "ignore_hidden_objects", toggle=True)
            if bpy.context.scene.export_settings.export_mode == 'Collections':
                row.prop(scene, "ignore_hidden_collections", toggle=True)

            row = box_export.row(align=True)
            row.prop(scene.export_settings, "export_mode")


            box_naming = box.box()
            row = box_naming.row()
            row.label(text="Naming Options")
            row = box_naming.row()
            row.prop(scene, "auto_sync_names", toggle=True)
            row = box_naming.row()
            row.prop(scene, "lod_names_capitalized", toggle=True)
            row = box_naming.row()
            row.prop(scene, "lod_name_lod0_ignore_postfix", toggle=True)
            row = box_naming.row()

            row.prop(scene, "lod_name_proxy", toggle=True)
            row = box_naming.row()
            row.prop(scene, "lod_name_use_prefix")
            if scene.lod_name_use_prefix:
                row.prop(scene, "lod_name_prefix", text = "")
        else:
            box.prop(scene, "export_options_show", icon="TRIA_RIGHT", emboss=False)

        row = layout.row()
        row.label(text="Assets", icon="CUBE")

        settings = context.scene.export_settings

        layout.template_list(
            "Asset_List",
            "asset_list",
            settings,
            "assets",
            settings,
            "active_asset_index",
            type='DEFAULT',
            rows=4
        )

        row = layout.row(align=True)
        #row.operator("eksportor.assets_unisolate")
        #row = layout.row(align=True)
        row.operator("eksportor.asset_add")
        row.operator("eksportor.asset_export_all")
        layout.label(text="LODs", icon="PRESET")

        # Does it even make sense to show data if there aren't
        # any assets created?

        if not settings.active_asset_index >= 0:
            return

        asset = settings.assets[settings.active_asset_index]
        lod = asset.lods[asset.active_lod_index]

        if scene.auto_sync_names:
            bpy.ops.eksportor.asset_lod_sync_all('INVOKE_DEFAULT')


        # Warning panel
        warnings = []

        for idx, lod in enumerate(asset.lods):
            warning = self.check_lod_is_empty(context, idx)
            if warning:
                warnings.append(warning)

        #warning = self.check_are_lods_descending(context)
        #if warning:
        #    warnings.append(warning)
        warning = self.check_too_many_lods(asset)
        if warning:
            warnings.append(warning)

        if len(warnings) > 0:
            box = layout.box()
            row = box.row()
            row.label(text="Warnings", icon='ERROR')

            for warn in warnings:
                self.show_warning(box, warn)

        row = layout.row()

        self.sync_lod_indexes(context)

        layout.template_list(
            "LOD_List",
            "lod_list",
            asset,
            "lods",
            asset,
            "active_lod_index",
            type='DEFAULT',
            rows=0
        )

        row = layout.row(align=True)
        row.operator("eksportor.asset_lod_add")

        column = row.column(align=True)

        column.operator("eksportor.asset_lod_remove_all")
        if not asset.does_have_lods(context):
            column.enabled = False

        if not scene.auto_sync_names:
            row = layout.row(align=True)
            row.operator("eksportor.asset_lod_sync")

        box = layout.box()
        if scene.show_lod_info:
            box.prop(scene, "show_lod_info", icon="TRIA_DOWN", text="LOD Info", emboss=False)
            row = box.row()
            row.label(text="Name")
            row.label(text=lod.name)

            row = box.row()
            row.label(text="Overall triangle count")
            vertcount = lod.calculate_vertcount(context)
            tricount = lod.calculate_tricount(context)

            row.label(text=str(tricount))
            row = box.row()
            row.label(text="Overall vertex count")
            row.label(text=str(vertcount))

            box = box.box()
            if scene.show_lod_contents:
                box.prop(scene, "show_lod_contents", icon="TRIA_DOWN", text="LOD Contents", emboss=False)

                title = "Add Collection"

                active_collection = bpy.context.view_layer.active_layer_collection
                in_use_count = 0
                i = 0
                for idx, lod in enumerate(asset.lods):
                    if lod.collections.get(active_collection.name) is not None:
                        in_use_count += 1
                        i = idx
                if in_use_count == 1:
                    title += " (already in use by LOD{index})".format(index=i)
                elif in_use_count > 1:
                    title += " (already in use by {count} LODs)".format(count=in_use_count)

                lod = asset.lods[asset.active_lod_index]
                box.operator("eksportor.asset_lod_collection_add", text=title)
                box.label(text="Collections ({count})".format(count = len(lod.collections)))
                box.template_list(
                    "LOD_Collections_List",
                    "lod_collections_list",
                    lod,
                    "collections",
                    lod,
                    "active_lod_collection_index",
                    type='DEFAULT',
                    rows=3
                )



                # Objects menu

                include_button_title = "Include Separate Object"

                row_buttons = box.row()
                row_other = layout.row()
                mesh_count = 0
                for ob in bpy.context.selected_objects:
                    obj_type = getattr(ob, 'type', '')
                    if obj_type != 'CAMERA' or obj_type != 'LIGHT':
                        mesh_count += 1

            #    if mesh_count > 1:
            #        include_button_title += "s"
            #    elif mesh_count == 0:
            #        row_buttons.enabled = False
                row_buttons.operator("eksportor.asset_lod_object_include", text=include_button_title)
                #if len(lod.objects) > 0:
                #if lod.active_lod_object_index >= 0:
                #selected_object = lod.objects[lod.active_lod_object_index]
                #if selected_object:
                #     mesh = selected_object.obj_pointer.data
                #     polys = str(len(mesh.polygons))
                #     verts = str(len(mesh.vertices))
                #     name = selected_object.obj_pointer.name
                #     box.label(text="Selected object [{name}] info: {polys} polys, {verts} verts.".format(name=name, polys=polys, verts=verts))
                # else:
                #     box.label(text="Objects")
                #else:
                #    box.label(text="Objects")


                box.template_list(
                    "LOD_Objects_List",
                    "lod_objects_list",
                    lod,
                    "objects",
                    lod,
                    "active_lod_object_index",
                    type='DEFAULT',
                    rows=3
                )
                row = box.row(align=True)

                # Object details
                #box_detail = box.box()
                #box_detail.label(text="Object details")
                #box_detail.label(text="Object details")
                #box_detail.label(text="Object details")



                row_other.operator("eksportor.asset_lod_object_select_all")
                row_other.operator("eksportor.asset_lod_content_remove_all")

            else:
                box.prop(scene, "show_lod_contents", icon="TRIA_RIGHT", text="LOD Contents", emboss=False)

        else:
            box.prop(scene, "show_lod_info", icon="TRIA_RIGHT", text="LOD Info", emboss=False)


class Eksporteur_Export_Popup(bpy.types.Operator, Eksporteur_Export_Base):
    bl_label = "Popup"
    bl_idname = "eksporteur.asset_export_popup"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"

    def draw(self, context):
        Eksporteur_Export_Base.draw(self, context)

    def execute(self, context):
        """
        Show Eksporteur as popup dialog.
        """
        return context.window_manager.invoke_popup(self)


class Eksporteur_Export_PT(Eksporteur_Export_Base):
    bl_label = "Ekspørtør Asset Export"
    bl_idname = "eksportor.asset_export"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "output"


    def draw(self, context):
        Eksporteur_Export_Base.draw(self, context)

class CreateWindow(bpy.types.Operator):
    bl_idname = "eksportor.create_window"
    bl_label = "Create Window"
    bl_options = set()

    def execute(self, context):
        # Call user prefs window
        bpy.ops.screen.userpref_show('INVOKE_DEFAULT')

        # Change area type
        area = bpy.context.window_manager.windows[-1].screen.areas[0]
        area.type = 'PROPERTIES'
        return {'FINISHED'}

class MoveUp(bpy.types.Operator):
    bl_idname = "eksportor.asset_move_up"
    bl_label = ""
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets.move(self.index, self.index - 1)
        return {'FINISHED'}

class MoveDown(bpy.types.Operator):
    bl_idname = "eksportor.asset_move_down"
    bl_label = ""
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets.move(self.index, self.index + 1)
        return {'FINISHED'}

class AddAsset(bpy.types.Operator):
    bl_idname = "eksportor.asset_add"
    bl_label = "Add Asset"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets.add()
        asset.init(context)
        settings.active_asset_index = len(settings.assets) - 1
        settings.get_current_asset(context).add_lod(context)
        return {'FINISHED'}

class RemoveAsset(bpy.types.Operator):
    bl_idname = "eksportor.asset_remove"
    bl_label = "Remove Active Asset"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        return context.scene.export_settings.active_asset_index >= 0

    def execute(self, context):
        settings = context.scene.export_settings
        settings.assets.remove(self.index)
        if settings.active_asset_index >= len(settings.assets):
            settings.active_asset_index = len(settings.assets) - 1
        return {'FINISHED'}

class ExportAsset(bpy.types.Operator):
    bl_idname = "eksportor.asset_export"
    bl_label = "Export"
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[self.index]

        print("Export Asset with index: " + str(self.index))
        print("Iterating through all Asset {name} LODs...".format(name=asset.name))

        for idx, lod in enumerate(settings.assets[self.index].lods):
            bpy.ops.eksportor.asset_lod_export(asset_index=self.index, lod_index=idx)
        return {'FINISHED'}

class ExportAllAssets(bpy.types.Operator):
    bl_idname = "eksportor.asset_export_all"
    bl_label = "Export All"
    bl_options = set()

    @classmethod
    def poll(cls, context):
        settings = context.scene.export_settings
        return len(settings.assets) > 0 and len(context.scene.content_folder) > 0

    def execute(self, context):
        settings = context.scene.export_settings
        assets = settings.assets
        # lods = xmodel.lods

        for idx, asset in enumerate(assets):
            bpy.ops.eksportor.asset_export(index=idx)

        # for lod in lods:
        #   print("LOD :: " + lod.name + ":: exported.")

        print("All Assets have been exported.")
        return {'FINISHED'}

# REMOVE?
class ExportAllLODs(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_export_all"
    bl_label = "Export All LODs"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        lods = asset.lods

        for lod in lods:
            print("LOD :: " + lod.name + ":: exported.")
        return {'FINISHED'}

class CollectionState:
    collection_name = None
    was_visible = True
    was_selected = False

    def restore(self, context):
        g.SetCollectionViewportVisibility(context, self.collection_name, self.was_visible)

    def print(self):
        pass
        #print("____ Printing Collection State _____")
        #print("Collection: " + self.collection_name)
        #print("Was visible? " + str(self.was_visible))
        #print("")
# Class for storing object state
# in order to restore it after export.
class ObjectState:
    pointer = None
    is_selected = False
    is_hidden = False
    location: Vector
    rotation: Euler
    belongs_to_collection = True

    def __init__(self, obj):
        self.pointer = obj
        self.is_selected = obj.select_get()
        self.is_hidden = obj.hide_get()
        self.location = Vector(obj.location)
        self.rotation = Euler(obj.rotation_euler)

    def restore(self):
        self.pointer.location = self.location
        self.pointer.rotation_euler = self.rotation
        self.pointer.hide_set(self.is_hidden)
        self.pointer.select_set(state=self.is_selected)

    def print(self):
        pass
        # print("____ Printing Object State _____")
        # print("Object: " + self.pointer.name)
        # print("Was selected? " + str(self.is_selected))
        # print("Was hidden? " + str(self.is_hidden))
        # print("Original Location: " + str(self.location))
        # print("Original Rotation: " + str(self.rotation))
        # print("Belongs to Collection: " + str(self.belongs_to_collection))
        # print("")

class ExportLOD(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_export"
    bl_label = "Export"
    bl_options = set()
    asset_index = IntProperty(default=0)
    lod_index = IntProperty(default=0)


    def execute(self, context):


        settings = context.scene.export_settings
        asset = settings.assets[self.asset_index]
        lod = asset.lods[self.lod_index]

        # Storing selected objects.
        selected = bpy.context.selected_objects
        active = bpy.context.view_layer.objects.active

        filepath = g.GetFilePath(context, asset.name)

        if settings.export_mode == 'Collections':
            object_states = []
            collection_states = []

            collection_objects = []
            should_ignore_hidden = context.scene.ignore_hidden_collections

            for collection in lod.collections:
                col = g.FindCollection(collection.name)

                # Iterate through all collections and their children collections.
                collections_tree = g.GetNestedCollections(col)
                for c in collections_tree:

                    # Save state
                    state = CollectionState()
                    state.collection_name = c.name

                    # Figure out if collection is visible.
                    collection_visible = True
                    for object in c.objects:

                        # Basically, if object is NOT visible but EYE icon is on.
                        # That means that Collection is hidden.
                        if not object.hide_get() and not object.visible_get():
                            state.was_visible = False
                            break

                    if not should_ignore_hidden:
                        collection_states.append(state)


                objects = g.GetNestedObjectsFromCollection(col, 0, False)

                collection_objects.extend(objects)

                #if not should_ignore_hidden:
                    #g.SetCollectionViewportVisibility(context, collection.name, visibility = True)

            # Filling states with collection objects.
            for obj in collection_objects:
                # Do we have to even include it?
                if obj.hide_get() and context.scene.ignore_hidden_objects:
                    continue
                else:
                    state = ObjectState(obj)
                    object_states.append(state)

            # Filling states with additional separate objects.
            for lod_object in lod.objects:
                ob = lod_object.obj_pointer

                # Check if separate object is already in collection.
                if ob in collection_objects:
                    continue
                else:
                    # Also, ignore if ignore setting is on and eye disabled.
                    if ob.hide_get() and context.scene.ignore_hidden_objects:
                        continue
                    else:
                        state = ObjectState(ob)
                        state.belongs_to_collection = False
                        object_states.append(state)

            last = self.lod_index + 1 >= len(asset.lods)
            name = g.GenerateLODName(asset.name, context, self.lod_index, last)


            bpy.ops.object.select_all(action='DESELECT')

            # Make sure that collections are visible.
            for col in collection_states:
                if not col.was_visible:
                    g.SetCollectionViewportVisibility(context, col.collection_name, visibility = True)


            # Prepare objects for export.
            for state in object_states:
                state.pointer.hide_set(False)
                state.pointer.select_set(state=True)

            # Moving export objects to world origin if necessary.
            if context.scene.move_to_world_origin:
                bpy.ops.object.location_clear(clear_delta=False)
            if context.scene.clear_rotation:
                bpy.ops.object.rotation_clear(clear_delta=False)

            #bpy.ops.eksportor.lod_file_export(filepath=filepath, lod_name=name)


            bpy.ops.eksportor.lod_file_export(filepath=filepath, lod_name=name)

            # Restoring collections state after export.
            for state in collection_states:
                state.print()
                state.restore(context)
            # Restoring exported objects state after export.
            for state in object_states:
                state.print()
                state.restore()
        elif settings.export_mode == 'Empties':
            base_name = asset.name
            lod_name = g.GenerateLODName(base_name, context, self.lod_index)
            bpy.ops.object.select_all(action='DESELECT')
            #bpy.ops.object.location_clear(clear_delta=False)
            #for object in lod.objects:
            if len(lod.objects) > 0:
                object = lod.objects[0].obj_pointer
                if object is not None:
                    object.select_set(state=True)

                    if len(object.constraints) > 0:
                        print("object [{name}] has constraint(s), muting them.".format(name = object.name))
                        for constraint in object.constraints:
                            constraint.mute = True

                    bpy.context.view_layer.objects.active = object
                    print("--- Working with object [{name}] located in [{loc}]".format(name = object.name, loc = object.location))
                    print("--- lod name is defined as: {name}".format(name = lod_name))

                    location = object.location.copy()
                    object.location = (0, 0, 0)
                    bpy.ops.object.select_grouped(type='CHILDREN_RECURSIVE')

                    children_selected = bpy.context.selected_objects
                    for child in children_selected:
                        if child.type == 'CAMERA' or child.type == 'LIGHT' or child.display_type == 'BOUNDS' or child.display_type == 'WIRE':
                            child.select_set(state=False)
                        else:
                            for constraint in child.constraints:
                                constraint.mute = True



                    bpy.ops.eksportor.lod_file_export(filepath=filepath, lod_name = lod_name)


                    for child in children_selected:
                        if child.type == 'MESH':
                            for constraint in child.constraints:
                                constraint.mute = False

                    bpy.ops.object.select_all(action='DESELECT')




                    bpy.context.view_layer.objects.active = object

                    # Restoring object
                    object.select_set(state=True)
                    object.location = location
                    for constraint in object.constraints:
                        constraint.mute = False
                    bpy.ops.object.select_all(action='DESELECT')
                # Restore selection

                bpy.ops.object.select_all(action='DESELECT')
                for obj in selected:
                    obj.select_set(state=True)
                bpy.context.view_layer.objects.active = active

        return {'FINISHED'}

class AddLOD(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_add"
    bl_label = "Add LOD"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        settings.assets[settings.active_asset_index].add_lod(context)
        return {'FINISHED'}

class RemoveLOD(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_remove"
    bl_label = "Remove"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        return asset.active_lod_index >= 0

    def execute(self, context):
        settings = context.scene.export_settings
        settings.assets[settings.active_asset_index].remove_lod(context, self.index)
        return {'FINISHED'}

class RemoveAllLODs(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_remove_all"
    bl_label = "Clear All"

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        asset.clear_lods(context)
        return {'FINISHED'}

class IsolateAsset(bpy.types.Operator):
    bl_idname = "eksportor.asset_isolate"
    bl_label = "Isolate Asset"
    asset_index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[self.asset_index]
        print("-- Isolating Asset [{name}].".format(name = asset.name))

        return {'FINISHED'}

class UnisolateAssets(bpy.types.Operator):
    bl_idname = "eksportor.assets_unisolate"
    bl_label = "Unisolate Assets"
    asset_index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[self.asset_index]
        print("-- Unisolating Assets.".format(name = asset.name))

        for a in settings.assets:
            for col in a.lods[0].collections:
                print ("     unisolating {name}".format(name = col.name))
                g.SetCollectionViewportVisibility(context, col.name, False)


        return {'FINISHED'}


class IsolateLOD(bpy.types.Operator):
    bl_idname = "eksportor.lod_isolate"
    bl_label = "Isolate LOD"
    lod_index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[settings.active_asset_index]
        lod = asset.lods[self.lod_index]

        for a in settings.assets:
            for l in a.lods:
                if lod.name != l.name:
                    for col in l.collections:
                        g.SetCollectionViewportVisibility(context, col.name, False)

        for col in lod.collections:
            g.SetCollectionViewportVisibility(context, col.name, True)

        return {'FINISHED'}

class FocusAsset(bpy.types.Operator):
    bl_idname = "eksportor.asset_focus"
    bl_label = "Isolate Asset"
    asset_index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[self.asset_index]
        lod = asset.lods[0]
        objects = lod.objects

        bpy.ops.object.select_all(action='DESELECT')
        for object in objects:
            pointer = object.obj_pointer
            if pointer is not None:
                pointer.select_set(state=True)
            for child in pointer.children:
                child.select_set(state=True)

        for screen in bpy.data.screens:
            for area in (a for a in screen.areas if a.type == 'VIEW_3D'):
                for region in (r for r in area.regions if r.type == 'WINDOW'):
                    override = {'screen': screen, 'area': area, 'region': region}
                    bpy.ops.view3d.view_selected(override)

        bpy.ops.object.select_all(action='DESELECT')
        for object in objects:
            if object.obj_pointer is not None:
                pointer.select_set(state=True)

        return {'FINISHED'}
class IsolateAsset(bpy.types.Operator):
    bl_idname = "eksportor.asset_isolate"
    bl_label = "Isolate Asset"
    asset_index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.assets[self.asset_index]
        lod = asset.lods[0]

        for a in settings.assets:
            for l in a.lods:
                if lod.name != l.name:
                    for col in l.collections:
                        g.SetCollectionViewportVisibility(context, col.name, False)

        for col in lod.collections:
            g.SetCollectionViewportVisibility(context, col.name, True)

        return {'FINISHED'}


class SetLOD(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_set"
    bl_label = "Set"
    bl_options = set()
    lod_index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[self.lod_index]
        print("Setting LOD information for: " + lod.name + ".")

        # Setting LOD content.
        lod.objects.clear()

        for obj in bpy.context.selected_objects:
            print("Setting object for LOD : " + obj.name)
            obj_type = getattr(obj, 'type', '')
            if obj_type != 'CAMERA' or obj_type != 'LIGHT':
                object = lod.objects.add()
                object.obj_pointer = bpy.data.objects.get(obj.name)

        return {'FINISHED'}

class SyncLODNamesAll(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_sync_all"
    bl_label = "Sync LOD Names"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        for asset in settings.assets:
            index = 0
            for lod in asset.lods:
                last = index + 1 >= len(asset.lods)
                base_name = settings.get_current_asset(context).name
                lod.name = g.GenerateLODName(base_name, context, index, last)
                index += 1
        return {'FINISHED'}

class SyncLODNames(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_sync"
    bl_label = "Sync LOD Names"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        index = 0
        for lod in asset.lods:
            last = index + 1 >= len(asset.lods)
            base_name = asset.name
            lod.name = g.GenerateLODName(base_name, context, index, last)
            index += 1
        return {'FINISHED'}

class SelectContent(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_object_select"
    bl_label = "Select"
    bl_options = set()
    index = IntProperty(default=0)

    # @classmethod
    # def poll(cls, context):
    #     settings = context.scene.export_settings
    #     asset = settings.get_current_asset(context)
    #     lod = asset.lods[asset.active_lod_index]
    #     return lod.objects[cls.index].obj_pointer is not None
        #return bpy.data.objects.get(object_to_select.name) is not None


    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]

        #lod.active_lod_object_index = self.index

        bpy.ops.object.select_all(action='DESELECT')

        object_to_select = lod.objects[self.index].obj_pointer

        #bpy.context.view_layer.objects.active = object_to_select
        #if bpy.data.objects.get(object_to_select.name) is not None:
        if object_to_select is not None:
            object_to_select.select_set(state=True)
        return {'FINISHED'}

class SelectAllContent(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_object_select_all"
    bl_label = "Select All"
    bl_options = set()

    @classmethod
    def poll(cls, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]
        return len(asset.get_lod_objects(context)) > 0

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]
        bpy.ops.object.select_all(action='DESELECT')

        # for content in lod.content:
        #     content_to_select = content.obj_pointer
        #     bpy.context.view_layer.objects.active = content_to_select
        #     content_to_select.select_set(state=True)
        return {'FINISHED'}

class IncludeObject(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_object_include"
    bl_label = "Include Object"
    bl_options = set()

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]

        for obj in bpy.context.selected_objects:
            obj_type = getattr(obj, 'type', '')
            if obj_type != 'CAMERA' and obj_type != 'LIGHT':
                bAlreadyIncluded = False
                for item in lod.objects:
                    o = item.obj_pointer
                    #if o.data == obj.data:
                    if o.name == obj.name:
                        bAlreadyIncluded = True
                        #print("Already Included")

                if not bAlreadyIncluded:
                    #print("Included")
                    object = lod.objects.add()
                    object.obj_pointer = bpy.data.objects.get(obj.name)
        return {'FINISHED'}

class RemoveObject(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_object_remove"
    bl_label = "Remove"
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]
        lod.remove_object_by_index(context, self.index)
        print("Remove selected LOD Object with index: " + str(self.index))
        return {'FINISHED'}

# Collections
class AddCollection(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_collection_add"
    bl_label = "Add Collection"
    bl_options = set()
    lod_index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]

        active_collection = bpy.context.view_layer.active_layer_collection
        if lod.collections.get(active_collection.name) is None:
            return True
        return False

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]
        print("Adding LOD information for: " + lod.name + ".")
        #Check selected collection in outliner.
        active_collection = bpy.context.view_layer.active_layer_collection

        if lod.collections.get(active_collection.name) is None:
            collection = lod.collections.add()
            collection.name = active_collection.name

            print("Added new collection to {lod}: {collection}".format(lod=lod.name, collection=collection.name))

        print("Collections size: {size}".format(size=str(len(lod.collections))))
        return {'FINISHED'}

class SelectCollection(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_collection_select"
    bl_label = "Select Collection"
    bl_options = set()
    index = IntProperty(default=0)

    @classmethod
    def poll(cls, context):
        return True

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]

        layer_collection = bpy.context.view_layer.layer_collection

        collection_name = lod.collections[self.index].name

        found_collection = g.FindCollection(collection_name)
        if found_collection:
            col = g.RecursiveSearchLayerCollection(layer_collection, collection_name)
            if col:
                bpy.context.view_layer.active_layer_collection = col

        return {'FINISHED'}

class RemoveCollection(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_collection_remove"
    bl_label = "Remove Collection"
    bl_options = set()
    index = IntProperty(default=0)

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]
        lod.collections.remove(self.index)
        return {'FINISHED'}


class RemoveAllContent(bpy.types.Operator):
    bl_idname = "eksportor.asset_lod_content_remove_all"
    bl_label = "Clear Content"
    bl_options = set()

    @classmethod
    def poll(cls, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]
        return len(asset.get_lod_objects(context)) > 0

    def execute(self, context):
        settings = context.scene.export_settings
        asset = settings.get_current_asset(context)
        lod = asset.lods[asset.active_lod_index]

        lod.objects.clear()
        lod.collections.clear()
        lod.active_lod_object_index = -1
        lod.active_lod_collection_index = -1

        print("Remove all LOD Content")
        return {'FINISHED'}

class OpenExportFolder(bpy.types.Operator):
    bl_idname = "eksportor.open_export_folder"
    bl_label = "Open Export Folder"
    bl_options = set()

    def execute(self, context):
        path = ""
        if context.scene.specific_export_path:
            path = context.scene.content_folder
        else:
            path = bpy.path.abspath("//")
        subprocess.Popen('explorer {path}'.format(path = path))
        return {'FINISHED'}



addon_keymaps = []

classes = (Asset_List,
        CreateWindow,
        LOD_List,
        LOD_Objects_List,
        LOD_Collections_List,
        Eksporteur_Export_Base,
        Eksporteur_Export_PT,
        Eksporteur_Export_Popup,
        AddAsset,
        RemoveAsset,
        ExportAsset,
        ExportAllAssets,
        RemoveAllLODs,
        ExportLOD,
        IsolateLOD,
        IsolateAsset,
        UnisolateAssets,
        FocusAsset,
        AddLOD,
        SetLOD,
        RemoveLOD,
        ExportAllLODs,
        AddCollection,
        SyncLODNames,
        SyncLODNamesAll,
        SelectContent,
        SelectCollection,
        SelectAllContent,
        IncludeObject,
        RemoveObject,
        RemoveCollection,
        RemoveAllContent,
        OpenExportFolder,
        MoveUp,
        MoveDown)


def register():
    for cls in classes:
        bpy.utils.register_class(cls)

    #bpy.types.WindowManager.interface_vars = bpy.props.PointerProperty(type=InterfaceVars)

    bpy.types.Scene.export_options_show = bpy.props.BoolProperty(name='Options show', default=False)
    bpy.types.Scene.content_folder = bpy.props.StringProperty(name='Content folder', default="", subtype='DIR_PATH')
    bpy.types.Scene.show_lod_info = bpy.props.BoolProperty(name='Show LOD info', default=True)
    bpy.types.Scene.show_lod_contents = bpy.props.BoolProperty(name='Show LOD Contents', default=True)
    bpy.types.Scene.lod_name_use_prefix = bpy.props.BoolProperty(name='Use Prefix', default=True)
    bpy.types.Scene.lod_name_prefix = bpy.props.StringProperty(name='Name Prefix', default="SM")
    bpy.types.Scene.lod_name_proxy = bpy.props.BoolProperty(name='Name last LOD as LODX', default=True)
    bpy.types.Scene.lod_name_lod0_ignore_postfix = bpy.props.BoolProperty(name='Ignore postfix for LOD0', default=True)
    bpy.types.Scene.lod_names_capitalized = bpy.props.BoolProperty(name='Capitalize LOD Names', default=True)
    bpy.types.Scene.auto_sync_names = bpy.props.BoolProperty(name='Auto Sync LOD Names',
                                                             description="Automaticly rename LODs according to Asset name",
                                                             default=True)
    bpy.types.Scene.move_to_world_origin = bpy.props.BoolProperty(name='Move to World Origin', description="Moves objects to world origin on export." , default=True)
    bpy.types.Scene.clear_rotation = bpy.props.BoolProperty(name='Clear Rotation', description="Clear objects rotations on export." , default=False)


    bpy.types.Scene.specific_export_path = bpy.props.BoolProperty(name='Specific Export Path', default=False)
    bpy.types.Scene.folder_per_asset = bpy.props.BoolProperty(name='Folder Per Asset', default=False)
    bpy.types.Scene.open_folder_after_export = bpy.props.BoolProperty(name='Open folder after export', default=False)

    bpy.types.Scene.ignore_hidden_objects = bpy.props.BoolProperty(name='Ignore hidden objects', default=False)
    bpy.types.Scene.ignore_hidden_collections = bpy.props.BoolProperty(name='Ignore hidden collections', default=False)


    wm = bpy.context.window_manager
    km = wm.keyconfigs.addon.keymaps.new(name='Object Mode', space_type='EMPTY')
    kmi = km.keymap_items.new('eksporteur.asset_export_popup', 'F7', 'PRESS')
    #kmi.properties.name = 'WARPTOOLS_MT_Menu_Tools'
    addon_keymaps.append(km)


def unregister():
    for cls in classes:
        bpy.utils.unregister_class(cls)

    del bpy.types.Scene.content_folder
    del  bpy.types.Scene.show_lod_info
    del  bpy.types.Scene.show_lod_contents
    del bpy.types.Scene.export_options_show
    del bpy.types.Scene.lod_name_use_prefix
    del bpy.types.Scene.lod_name_prefix
    del bpy.types.Scene.lod_name_proxy
    del bpy.types.Scene.lod_names_capitalized
    del bpy.types.Scene.lod_name_lod0_ignore_postfix
    del bpy.types.Scene.auto_sync_names
    del bpy.types.Scene.move_to_world_origin
    del bpy.types.Scene.clear_rotation
    del bpy.types.Scene.folder_per_asset
    del bpy.types.Scene.ignore_hidden_objects
    del bpy.types.Scene.ignore_hidden_collections



if __name__ == "__main__":
    register()
